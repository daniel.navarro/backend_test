package com.glovoapp.backender;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class OrderByCourierControllerTest {

	@Mock
	CourierRepository courierRepository;
	@Mock
	OrderRepository orderRepository;

	private OrderByCourierController orderByCourierController;

	private Courier courier;
	private List<Order> orderList;
	private List<String> keyWordBoxSet = new ArrayList<>();
	private List<String> criteriaSort = new ArrayList<>();
	private Location francescMacia = new Location(41.3925603, 2.1418532);
	private Location placaCatalunya = new Location(41.3870194, 2.1678584);
	private Location tibidabo = new Location(41.4225, 2.1186111111111);
	private Location urquinaona = new Location(41.38944, 2.17333);
	private Double courierDistance;
	private Double slotDistance;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		keyWordBoxSet.add("pizza");
		keyWordBoxSet.add("caKe");
		courierDistance = 5.0;
		slotDistance = 0.5;
		criteriaSort.add("vip");
		criteriaSort.add("food");
		orderByCourierController = new OrderByCourierController(courierRepository, orderRepository, keyWordBoxSet,
				courierDistance, slotDistance, criteriaSort);

		courier = new Courier();
		courier.withId("courierId");
		courier.withBox(true); // courier with Box -> can see all orders
		courier.withVehicle(Vehicle.MOTORCYCLE); // courier that can see others are futher than 5km
		courier.withLocation(placaCatalunya);// position courier Center Barcelona

		orderList = new ArrayList<>();
		Order order = new Order();
		order.withId("order-1");
		order.withDescription(" Pizza xxxxx");
		order.withVip(Boolean.FALSE);
		order.withFood(Boolean.TRUE);
		order.withPickup(placaCatalunya); // pickup location less than 5km from courier location
		order.withDelivery(francescMacia); // delivery location less than 5km from courier location
		orderList.add(order);
		order = new Order();
		order.withId("order-2");
		order.withDescription("Description without key words");
		order.withVip(Boolean.TRUE);
		order.withFood(Boolean.FALSE);
		order.withPickup(francescMacia); // pickup location less than 5km from courier location
		order.withDelivery(urquinaona); // delivery location less than 5km from courier location
		orderList.add(order);
		// discarded below others for courier without motorcycle or electric scooter
		order = new Order();
		order.withId("order-3");
		order.withDescription("Description CAKE ");
		order.withVip(Boolean.TRUE);
		order.withFood(Boolean.TRUE);
		order.withPickup(tibidabo); // pickup location more than 5km from courier location
		order.withDelivery(urquinaona); // delivery location less than 5km from courier location
		orderList.add(order);
		order = new Order();
		order.withId("order-4");
		order.withDescription("Description without key words");
		order.withVip(Boolean.FALSE);
		order.withFood(Boolean.FALSE);
		order.withPickup(francescMacia); // pickup location less than 5km from courier location
		order.withDelivery(tibidabo); // delivery location more than 5km from courier location
		orderList.add(order);

	}

	@Test
	void testOrderBoxTrueAndMotorcycleMore5km() {
		when(courierRepository.findById(any())).thenReturn(courier);
		when(orderRepository.findAll()).thenReturn(orderList);

		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		assertNotNull(ordersResult);
		assertEquals(4, ordersResult.size());
		// first slot
		assertEquals("order-2", ordersResult.get(0).getId());
		assertEquals("order-1", ordersResult.get(1).getId());
		// second slot
		assertEquals("order-3", ordersResult.get(2).getId());
		assertEquals("order-4", ordersResult.get(3).getId());
	}

	@Test
	void testOrderBoxTrueAndBycicleLess5km() {
		when(courierRepository.findById(any())).thenReturn(courier);
		when(orderRepository.findAll()).thenReturn(orderList);

		courier.withVehicle(Vehicle.BICYCLE);

		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		assertNotNull(ordersResult);
		assertEquals(2, ordersResult.size());
		// first slot
		assertEquals("order-2", ordersResult.get(0).getId());
		assertEquals("order-1", ordersResult.get(1).getId());
	}

	@Test
	void testOrderBoxFalseAndMotorcycleMore5km() {
		when(courierRepository.findById(any())).thenReturn(courier);
		when(orderRepository.findAll()).thenReturn(orderList);

		// without box this courier can't see order with keyWords word's
		courier.withBox(false);

		when(courierRepository.findById(any())).thenReturn(courier);
		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		assertNotNull(ordersResult);
		assertEquals(2, ordersResult.size());
		for (OrderVM orderVM : ordersResult) {
			assertEquals("Description without key words", orderVM.getDescription());
		}

	}

	@Test
	void testOrderBoxFalseAndBicycleLess5km() {
		when(courierRepository.findById(any())).thenReturn(courier);
		when(orderRepository.findAll()).thenReturn(orderList);

		// without box this courier can't see order with keyWords word's
		courier.withBox(false);
		courier.withVehicle(Vehicle.BICYCLE);

		when(courierRepository.findById(any())).thenReturn(courier);
		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		assertNotNull(ordersResult);
		assertEquals(1, ordersResult.size());
		assertEquals("Description without key words", ordersResult.get(0).getDescription());
		assertEquals("order-2", ordersResult.get(0).getId());
	}

	@Test
	void testCourierIsMissing() {
		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		courier.withVehicle(Vehicle.BICYCLE);
		assertTrue(ordersResult.isEmpty());
		verify(courierRepository, times(1)).findById("courierId");
		verify(orderRepository, never()).findAll();
	}

	@Test
	void testKeyWordsAreMissing() {
		courier.withBox(false);
		when(courierRepository.findById(any())).thenReturn(courier);
		when(orderRepository.findAll()).thenReturn(orderList);
		keyWordBoxSet = new ArrayList<>();
		orderByCourierController = new OrderByCourierController(courierRepository, orderRepository, keyWordBoxSet,
				courierDistance, slotDistance, criteriaSort);
		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		assertNotNull(ordersResult);
		assertEquals(4, ordersResult.size());
	}

	@Test
	void testCriteriaIsMissing() {
		when(courierRepository.findById(any())).thenReturn(courier);
		when(orderRepository.findAll()).thenReturn(orderList);
		criteriaSort = new ArrayList<>();
		orderByCourierController = new OrderByCourierController(courierRepository, orderRepository, keyWordBoxSet,
				courierDistance, slotDistance, criteriaSort);
		List<OrderVM> ordersResult = orderByCourierController.orders("courierId");
		assertNotNull(ordersResult);
		assertEquals(4, ordersResult.size());
	}
}
