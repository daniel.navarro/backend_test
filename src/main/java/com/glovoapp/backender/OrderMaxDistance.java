package com.glovoapp.backender;

public class OrderMaxDistance extends Order {

	private Double maxDistance;

	public Double getMaxDistance() {
		return maxDistance;
	}

	public void setMaxDistance(Double maxDistance) {
		this.maxDistance = maxDistance;
	}

	public void setOrder(Order order) {
		this.withDelivery(order.getDelivery());
		this.withDescription(order.getDescription());
		this.withFood(order.getFood());
		this.withId(order.getId());
		this.withPickup(order.getPickup());
		this.withVip(order.getVip());
	}

	@Override
	public String toString() {
		return "OrderMaxDistance [maxDistance=" + maxDistance + "] Vip[" + super.getVip() + "] Food[" + super.getFood()
				+ "] Description [" + super.getDescription() + "] id[" + super.getId() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((maxDistance == null) ? 0 : maxDistance.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderMaxDistance other = (OrderMaxDistance) obj;
		if (maxDistance == null) {
			if (other.maxDistance != null)
				return false;
		} else if (!maxDistance.equals(other.maxDistance))
			return false;
		return true;
	}

}
