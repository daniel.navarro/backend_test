package com.glovoapp.backender;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author dani
 *
 */
@Controller
class OrderByCourierController {

	private final CourierRepository courierRepository;
	private final OrderRepository orderRepository;
	/**
	 * List of works to filter orders for Courier with Glovo Box
	 */
	private List<String> keyWordBoxSet;
	private Double courierDistance;
	private Double slotDistance;
	private List<String> orderCriteria;

	@Autowired
	OrderByCourierController(CourierRepository courierRepository, OrderRepository orderRepository,
			@Value("#{'${courier.box.key_word}'.split(',')}") List<String> keyWordBoxSet,
			@Value("${order.courier.distance}") Double courierDistance,
			@Value("${order.slot.distance}") Double slotDistance,
			@Value("#{'${order.slot.criteria}'.split(',')}") List<String> orderCriteria) {
		this.courierRepository = courierRepository;
		this.orderRepository = orderRepository;
		this.keyWordBoxSet = keyWordBoxSet.stream().filter(s -> !s.isEmpty()).map(String::toUpperCase)
				.collect(Collectors.toList());
		this.courierDistance = courierDistance;
		this.slotDistance = slotDistance;
		this.orderCriteria = orderCriteria.stream().filter(s -> !s.isEmpty()).collect(Collectors.toList());
	}

	@RequestMapping(value = "/orders/{courierId}", method = RequestMethod.GET)
	@ResponseBody
	List<OrderVM> orders(@PathVariable("courierId") String courierId) {

		List<Order> orderList;
		List<OrderVM> orderVMList = new ArrayList<>();

		Courier courier = courierRepository.findById(courierId);
		if (courier != null) {
			orderList = orderRepository.findAll();
			orderList = filterByBoxAndDistance(courier, orderList);
			return slotingAndOrdering(courier, orderList);
		}
		return orderVMList;

	}

	/**
	 * If the description of the order contains the words {courier.box.key_word}, we
	 * can only show the order to the courier if they are equipped with a Glovo box.
	 * If the order is further than 5km to the courier, we will only show it to
	 * couriers that move in motorcycle or electric scooter.
	 * 
	 * @param courier
	 * @param orders
	 * @return
	 */
	private List<Order> filterByBoxAndDistance(Courier courier, List<Order> orders) {
		// if courier doesn't have Blovo Box the code will hidden order that contains
		// this words
		if (!courier.getBox()) {
			orders = orders.stream().filter(
					c -> keyWordBoxSet.stream().parallel().noneMatch(c.getDescription().toUpperCase()::contains))
					.collect(Collectors.toList());
		}
		if (Vehicle.BICYCLE.equals(courier.getVehicle())) {
			// to clean the orders with pickup or delivery location are futher than 5km
			orders = orders.stream().filter(o -> !isFutherThanDistance(courier, o)).collect(Collectors.toList());
		}
		return orders;
	}

	private Boolean isFutherThanDistance(Courier courier, Order order) {
		Boolean result = Boolean.FALSE;
		if ((DistanceCalculator.calculateDistance(courier.getLocation(), order.getDelivery()) > courierDistance)
				|| (DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup()) > courierDistance)) {
			result = Boolean.TRUE;
		}
		return result;
	}

	private List<OrderVM> slotingAndOrdering(Courier courier, List<Order> orders) {
		// I suppose that the distance slot is the maximum distance between courier and
		// pickup or delivery location. The worst case.
		List<OrderVM> orderFinal = new ArrayList<>();
		HashMap<Integer, List<OrderMaxDistance>> slotMap = slotFlow(courier, orders);

		/*
		 * System.out.println("previous order"); for (Map.Entry<Integer,
		 * List<OrderMaxDistance>> entry : slotMap.entrySet()) {
		 * System.out.println(entry.getKey()); for (OrderMaxDistance orderMax :
		 * entry.getValue()) { System.out.println(orderMax.toString()); } }
		 */

		if (!orderCriteria.isEmpty()) {
			for (Map.Entry<Integer, List<OrderMaxDistance>> entry : slotMap.entrySet()) {
				List<OrderMaxDistance> orderWithCriteria = new ArrayList<>();
				for (String criteria : orderCriteria) {
					if (entry.getValue() != null && !criteria.isEmpty()) {
						Predicate<OrderMaxDistance> predicateCriteria = OrderCriteria.getFilter(criteria);
						orderWithCriteria.addAll(entry.getValue().stream().filter(predicateCriteria)
								.sorted(Comparator.comparing(OrderMaxDistance::getMaxDistance))
								.collect(Collectors.toList()));
						entry.getValue().removeIf(OrderCriteria.getFilter(criteria));
					} else {
						break;
					}
				}
				if (entry.getValue() != null && !entry.getValue().isEmpty()) {
					orderWithCriteria.addAll(
							entry.getValue().stream().sorted(Comparator.comparing(OrderMaxDistance::getMaxDistance))
									.collect(Collectors.toList()));
				}
				slotMap.put(entry.getKey(), orderWithCriteria);
			}

		} else {
			// only order by distance
			for (Map.Entry<Integer, List<OrderMaxDistance>> entry : slotMap.entrySet()) {
				slotMap.put(entry.getKey(), entry.getValue().stream()
						.sorted(Comparator.comparing(OrderMaxDistance::getMaxDistance)).collect(Collectors.toList()));
			}
		}

		for (Map.Entry<Integer, List<OrderMaxDistance>> entry : slotMap.entrySet()) {
			for (OrderMaxDistance orderMax : entry.getValue()) {
				// System.out.println(orderMax.toString());
				orderFinal.add(new OrderVM(orderMax.getId(), orderMax.getDescription()));
			}
		}
		return orderFinal;

	}

	/**
	 * @param courier
	 * @param orders
	 * @return
	 */
	private HashMap<Integer, List<OrderMaxDistance>> slotFlow(Courier courier, List<Order> orders) {
		HashMap<Integer, List<OrderMaxDistance>> slotMap = new HashMap<>();
		for (Order order : orders) {
			Double maxDistance = Math.max(
					DistanceCalculator.calculateDistance(courier.getLocation(), order.getDelivery()),
					DistanceCalculator.calculateDistance(courier.getLocation(), order.getPickup()));
			Integer position = (int) (maxDistance / slotDistance);
			OrderMaxDistance orderMax = new OrderMaxDistance();
			orderMax.setOrder(order);
			orderMax.setMaxDistance(maxDistance);
			if (slotMap.get(position) == null) {
				List<OrderMaxDistance> listOrder = new ArrayList<OrderMaxDistance>();
				listOrder.add(orderMax);
				slotMap.put(position, listOrder);
			} else {
				slotMap.get(position).add(orderMax);
			}
		}
		return slotMap;
	}

}
